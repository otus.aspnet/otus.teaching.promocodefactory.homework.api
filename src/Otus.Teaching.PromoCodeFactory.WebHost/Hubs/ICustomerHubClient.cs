﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public interface ICustomerHubClient
    {
        /// <summary>
        /// Возвращает список клиентов.
        /// </summary>
        /// <returns></returns>
        public List<CustomerShortResponse> GetCustomersAsync();

        /// <summary>
        /// Возвращает клиента по id 
        /// </summary>
        /// <param name="id">id клиента</param>
        /// <returns></returns>
        public CustomerResponse GetCustomerAsync(Guid id);

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        /// <param name="request">Описание клиента.</param>
        /// <returns></returns>
        public CustomerResponse CreateCustomerAsync(CreateOrEditCustomerRequest request);

        /// <summary>
        /// Изменение клиента.
        /// </summary>
        /// <param name="id">id клиента.</param>
        /// <param name="request">Описание клиента.</param>
        public void EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request);

        /// <summary>
        /// Удаление клиента.
        /// </summary>
        /// <param name="id">id клиента.</param>
        public void DeleteCustomerAsync(Guid id);

    }
}
