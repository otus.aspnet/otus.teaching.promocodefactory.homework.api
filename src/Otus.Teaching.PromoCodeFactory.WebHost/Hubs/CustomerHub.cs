﻿using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public class CustomerHub : Hub<ICustomerHubClient>
    {
        private readonly ICustomerService _customerService;

        public CustomerHub(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        ///<inheritdoc/>
        public async  Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var response = await _customerService.GetCustomersAsync().ConfigureAwait(false);

            return response;

        }
        public async Task<CustomerResponse> GetCustomerAsync(Guid id)
        {
            var response = await _customerService.GetCustomerAsync(id).ConfigureAwait(false);

            return response;
        }
        public async Task<Guid> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var id = await _customerService.CreateCustomerAsync(request);

            return id;
        }
        public async Task EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            await _customerService.EditCustomersAsync(id, request);
        }
        public async Task DeleteCustomerAsync(Guid id)
        {
            await _customerService.DeleteCustomerAsync(id).ConfigureAwait(false);
        }

    }
}
