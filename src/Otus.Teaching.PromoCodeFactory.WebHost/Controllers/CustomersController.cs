﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService )
        {
            _customerService = customerService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var response = await _customerService.GetCustomersAsync().ConfigureAwait(false);

            return Ok(response);
        }
        
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var response = await _customerService.GetCustomerAsync(id).ConfigureAwait(false);

            return Ok(response);
        }
        
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var id = await _customerService.CreateCustomerAsync(request);

            return CreatedAtAction(nameof(CreateCustomerAsync), new {id = id }, id);
        }
        
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var idu = await _customerService.EditCustomersAsync( id,  request);
            if (idu == null)
                return NotFound();

            return NoContent();
        }
        
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {

            var guid = await _customerService.DeleteCustomerAsync(id).ConfigureAwait(false);
            if (guid == null)
                return NotFound();

            return NoContent();
        }
    }
}