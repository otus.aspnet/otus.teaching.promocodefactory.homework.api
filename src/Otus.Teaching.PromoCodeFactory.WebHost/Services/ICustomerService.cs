﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public interface ICustomerService
    {
        public Task<List<CustomerShortResponse>> GetCustomersAsync();
        public Task<CustomerResponse> GetCustomerAsync(Guid id);
        public Task<Guid> CreateCustomerAsync(CreateOrEditCustomerRequest request);
        public Task<Guid?> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request);
        public Task<Guid?> DeleteCustomerAsync(Guid id);

    }
}
