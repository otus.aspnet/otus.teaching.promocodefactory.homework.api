﻿using Microsoft.AspNetCore.SignalR;
using Moq;
using Otus.Teaching.PromoCodeFactory.WebHost.Hubs;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Hubs.Customer
{
    public class CustomerSignalRApiTest 
    {
        private Mock<ICustomerHubClient> _mockClientProxy = new Mock<ICustomerHubClient>();
        private Mock<IHubContext<CustomerHub, ICustomerHubClient>> _customerHub = new Mock<IHubContext<CustomerHub, ICustomerHubClient>>();
        private Mock<IHubClients<ICustomerHubClient>>  _mockClients = new Mock<IHubClients<ICustomerHubClient>>();


        private IHubContext<CustomerHub, ICustomerHubClient> getCustomerHub()
        {
            _mockClients.Setup(clients => clients.User(It.IsAny<string>())).Returns(_mockClientProxy.Object);
            _customerHub.Setup(x => x.Clients).Returns(() => _mockClients.Object);
            return _customerHub.Object;
        }

        [Fact]
        public void Call_GetCustomersAsync()
        {
            // arrange
            var customerHub = getCustomerHub();
            // act
            customerHub.Clients.User("identifier").GetCustomersAsync();
            // assert
            _mockClientProxy.Verify(x => x.GetCustomersAsync(), Times.Once);
        }

        [Fact]
        public void Call_GetCustomerAsync()
        {
            // arrange
            var customerHub = getCustomerHub();
            // act
            customerHub.Clients.User("identifier").GetCustomerAsync(It.IsAny<Guid>());
            // assert
            _mockClientProxy.Verify(x => x.GetCustomerAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public void Call_CreateCustomerAsync()
        {
            // arrange
            var customerHub = getCustomerHub();
            // act
            customerHub.Clients.User("identifier").CreateCustomerAsync(It.IsAny<CreateOrEditCustomerRequest>());
            // assert
            _mockClientProxy.Verify(x => x.CreateCustomerAsync(It.IsAny<CreateOrEditCustomerRequest>()), Times.Once);
        }

        [Fact]
        public void Call_EditCustomersAsync()
        {
            // arrange
            var customerHub = getCustomerHub();
            // act
            customerHub.Clients.User("identifier").EditCustomersAsync(It.IsAny<Guid>(),It.IsAny <CreateOrEditCustomerRequest>());
            // assert
            _mockClientProxy.Verify(x => x.EditCustomersAsync(It.IsAny<Guid>(), It.IsAny<CreateOrEditCustomerRequest>()), Times.Once);
        }

        [Fact]
        public void Call_DeleteCustomerAsync()
        {
            // arrange
            var customerHub = getCustomerHub();
            // act
            customerHub.Clients.User("identifier").DeleteCustomerAsync(It.IsAny<Guid>());
            // assert
            _mockClientProxy.Verify(x => x.DeleteCustomerAsync(It.IsAny<Guid>()), Times.Once);
        }

    }
}
